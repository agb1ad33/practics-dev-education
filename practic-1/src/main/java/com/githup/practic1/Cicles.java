package com.githup.practic1;

public class Cicles {

    public static void main(String[] args) {

        int i = 0;
        while (i >= -10) {
            System.out.println(i);
            i--;
        }
        System.out.println("**************");

        i = -20;
        while (i <= 20) {
            System.out.println(i);
            i++;
        }
        System.out.println("**************");

        i = 30;
        while (i >= 10) {
            System.out.println(i);
            i--;
        }
        System.out.println("**************");

        i = 1;
        while (i <= 100) {
            if (i % 7 == 0)
                System.out.println(i);
            i++;
        }
        System.out.println("**************");

        i = 1;
        while (i <= 100) {
            if (i % 5 == 0 || i % 3 == 0)
                System.out.println(i);
            i++;
        }
        System.out.println("**************");

        for (i = 0; i >= -10; i--) {
            System.out.println(i);
        }
        System.out.println("**************");

        for (i = -20; i <= 20; i++) {
            System.out.println(i);
        }
        System.out.println("**************");

        for (i = 30; i >= 10; i--) {
            System.out.println(i);
        }
        System.out.println("**************");

        for (i = 1; i <= 100; i++) {
            if (i % 7 == 0) {
                System.out.println(i);
            }
        }
        System.out.println("**************");

        for (i = 1; i <= 100; i++) {
            if (i % 3 == 0 || i % 5 == 0) {
                System.out.println(i);
            }
        }
    }
}
